<?php
/**
 * @file
 * This file provides administration form for the module.
 */

/**
 * Provides form for Webreep key control
 */
function webreep_admin_form() {
  $form = array();

  $form['webreep_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webreep'),
    '#collapsed' => FALSE,
    '#description' => t("Webreep provides you a code wich contains a part similar to:") . "<br>" .
                      "&nbsp;&nbsp;&nbsp;&nbsp;s.src = 'https://api.webreep.com/Feedback/Get/<b>fe138e9a-c75a-459e-95aa-4d94f12345f9</b>'" . "<br>" .
                      "Paste the bold part in the following field and click on save button.",
    );
  $form['webreep_fieldset']['webreep_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Webreep Key'),
    '#default_value' => variable_get("webreep_key", ""), 
  );
  $form['#submit'][]='webreep_config_submit';
  return system_settings_form($form);
}

function webreep_config_submit($form, &$form_state) {
  $key=$form_state['values']['webreep_key'];
  variable_set('webreep_key', $key);
}



function webreep_admin_form_validate($form, &$form_state) {
  $key=str_replace("-", "", $form_state['values']['webreep_key']);
  if (!ctype_alnum($key)) {
    form_set_error("webreep_key", t("Only dashes and alphanumeric characters are allowed."));
  }
}