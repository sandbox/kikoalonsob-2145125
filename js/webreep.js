/**
 * @file
 * Webreep module javascripts.
 */
(function ($) {
    
  Drupal.behaviors.webreep = {
    attach: function(context, settings) {
        function loadWebreep() {
            var s = document.createElement('script'); 
            s.type = 'text/javascript'; 
            s.async = true;
            s.src = 'https://api.webreep.com/Feedback/Get/'+Drupal.settings.webreep.webreep_key;
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (window.attachEvent) {
            window.attachEvent('onload', loadWebreep);
        }else{
            window.addEventListener('load', loadWebreep, false);
        }
    }
  }
})(jQuery);