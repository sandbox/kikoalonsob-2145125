
-- SUMMARY --

The Webreep Drupal module deploys a customer feedback survey solution on a
Drupal-based website.

For a full description of the module, visit the project page:
https://drupal.org/sandbox/kikoalonsob/2145125


-- REQUIREMENTS --

Drupal 7.x


-- INSTALLATION --

* Install as a usual sandbox module.


-- CONFIGURATION --
* Configure your Webreep Key in Administration » System » Webreep Configuration


-- CONTACT --

Current maintainer:
* KikoAlonsoB - fco.alonso88@gmail.com
